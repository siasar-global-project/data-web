import React, { Component } from 'react';

import EntityList from 'components/common/EntityList';

class HealthCenterList extends Component {
  render() {
    return (
      <EntityList
        entity="healthCenter"
        endpoint="health-centers"
        fields={['patients', 'staff', 'ecsCagi', 'ecsShsi']}
      />
    );
  }
}

export default HealthCenterList;
