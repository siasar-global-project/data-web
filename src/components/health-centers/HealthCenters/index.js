import React, { Component } from 'react';
import { entityColors } from 'lib/Utils';
import Entities from 'components/common/Entities';

class HealthCenters extends Component {
  render() {
    return (
      <Entities
        entity="healthCenter"
        endpoint="health-centers"
        indicator="ecsCsa"
        stats
        backgroundColor={entityColors[0]}
      />
    );
  }
}


export default HealthCenters;
