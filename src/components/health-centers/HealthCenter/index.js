import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Table, Card, CardBody } from 'reactstrap';
import { translate } from 'react-i18next';
import { Link } from 'react-router-dom';

import Api from 'lib/Api';
import { entityColors } from 'lib/Utils';

import PageHeader from 'components/common/PageHeader';
import SectionHeader from 'components/common/SectionHeader';
import ValueBox from 'components/common/ValueBox';
import Picture from 'components/common/Picture';
import PointMap from 'components/common/maps/PointMap';
import HealthCenterScoreTree from 'components/health-centers/HealthCenterScoreTree';
import InfoBox from 'components/common/InfoBox';
import CentersList from 'components/common/CentersList';

class HealthCenter extends Component {
  constructor(props) {
    super(props);

    const { match: { params } } = props;

    this.state = {
      id: parseInt(params.id, 10),
      healthCenter: {
        location: [],
        systems: [],
        communities: [],
      },
    };
  }

  componentDidMount() {
    Api.get(`health-centers/${this.state.id}`)
      .then(healthCenter => this.setState({ healthCenter }));
  }

  render() {
    const { t } = this.props;
    return (
      <React.Fragment>
        <PageHeader title={t('healthCenter')} />
        <Row>
          <Col>
            <InfoBox
              entity={this.state.healthCenter}
              entityName="healthCenter"
              showPdfButton={false}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="6" md="3">
            <ValueBox
              title={t('staff')}
              value={this.state.healthCenter.staff}
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('patients')}
              value={this.state.healthCenter.patients}
              color="community"
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('haveSystem')}
              value={t(this.state.healthCenter.haveSystem)}
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('sufficientWashingFacilities')}
              value={t(this.state.healthCenter.sufficientWashingFacilities)}
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="3">
            <HealthCenterScoreTree
              ecsCsa={this.state.healthCenter.ecsCsa}
              ecsCagi={this.state.healthCenter.ecsCagi}
              ecsShsi={this.state.healthCenter.ecsShsi}
            />
          </Col>
          <Col sm="12" md="6" xl="3">
            <Picture imgUrl={this.state.healthCenter.pictureUrl} />
          </Col>
          <Col sm="12" md="12" xl="6" className="d-flex">
            <PointMap
              latitude={this.state.healthCenter.latitude}
              longitude={this.state.healthCenter.longitude}
              score={this.state.healthCenter.score}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="6">
            <Card className="join-list font-sm">
              <h5>{t('community')}</h5>
              <CardBody className="p-0">
                <Table size="sm" className="table table-striped table-borderless" responsive>
                  <thead>
                    <tr>
                      <th className="col-md-8 col-sm-8">{t('name')}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr key={this.state.healthCenter.communities.siasarId}>
                      <td className="filterable-cell col-md-8">
                        <span
                          className={`circle-score background-${(this.state.healthCenter.communities.score)}`}
                        />
                        <Link to={`/communities/${this.state.healthCenter.communities.siasarId}`}>
                          { this.state.healthCenter.communities.name }
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
          <Col sm="12" md="6" xl="6">
            <Card className="join-list font-sm">
              <h5>{t('system')}</h5>
              <CardBody className="p-0">
                <Table size="sm" className="table table-striped table-borderless" responsive>
                  <thead>
                    <tr>
                      <th className="col-md-8 col-sm-8">{t('name')}</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.healthCenter.systems && (
                      <tr key={this.state.healthCenter.systems.siasarId}>
                        <td className="filterable-cell col-md-8">
                          <span
                            className={`circle-score background-${(this.state.healthCenter.systems.score)}`}
                          />
                          <Link to={`/systems/${this.state.healthCenter.systems.siasarId}`}>
                            { this.state.healthCenter.systems.name }
                          </Link>
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <section className="page-break">
          <SectionHeader title={t('specificData')} />
          <Row>
            <Col md="12" lg="6">
              <CentersList
                title={t('patient_plural')}
                items={[
                  {
                    title: `${t('femalePatients')}`,
                    data: this.state.healthCenter.femalePatients,
                  },
                  {
                    title: `${t('malePatients')}`,
                    data: this.state.healthCenter.malePatients,
                  },
                ]}
              />
            </Col>
            <Col md="12" lg="6">
              <CentersList
                title={t('water')}
                items={[
                  {
                    title: `${t('system')}`,
                    data: `${t(this.state.healthCenter.system)}`,
                  },
                ]}
              />
            </Col>
          </Row>
          <Row>
            <Col md="12" lg="6">
              <CentersList
                title={t('sanitation')}
                items={[
                  {
                    title: `${t('improvedFacilitiesTypeI')}`,
                    data: this.state.healthCenter.improvedFacilitiesTypeI,
                  },
                  {
                    title: `${t('improvedFacilitiesTypeIi')}`,
                    data: this.state.healthCenter.improvedFacilitiesTypeIi,
                  },
                  {
                    title: `${t('separateFacilitiesStaffPatients')}`,
                    data: `${t(this.state.healthCenter.separateFacilitiesStaffPatients)}`,
                  },
                  {
                    title: `${t('separateFacilitiesMenWoman')}`,
                    data: `${t(this.state.healthCenter.separateFacilitiesBoysGirls)}`,
                  },
                ]}
              />
            </Col>
            <Col md="12" lg="6">
              <CentersList
                title={t('hygiene')}
                items={[
                  {
                    title: `${t('installationsWaterSoap')}`,
                    data: this.state.healthCenter.installationsWaterSoap,
                  },
                  {
                    title: `${t('separateFacilitiesStaffPatientsHygiene')}`,
                    data: `${t(this.state.healthCenter.separateFacilitiesStaffPatientsHygiene)}`,
                  },
                ]}
              />
            </Col>
          </Row>
        </section>
      </React.Fragment>
    );
  }
}

HealthCenter.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }).isRequired,
  t: PropTypes.func.isRequired,
};

export default translate()(HealthCenter);
