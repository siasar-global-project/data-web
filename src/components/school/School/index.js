import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Table, Card, CardBody } from 'reactstrap';
import { translate } from 'react-i18next';
import { Link } from 'react-router-dom';

import Api from 'lib/Api';
import { entityColors } from 'lib/Utils';

import PageHeader from 'components/common/PageHeader';
import SectionHeader from 'components/common/SectionHeader';
import ValueBox from 'components/common/ValueBox';
import Picture from 'components/common/Picture';
import PointMap from 'components/common/maps/PointMap';
import SchoolScoreTree from 'components/school/SchoolScoreTree';
import InfoBox from 'components/common/InfoBox';
import CentersList from 'components/common/CentersList';

class School extends Component {
  constructor(props) {
    super(props);

    const { match: { params } } = props;

    this.state = {
      id: parseInt(params.id, 10),
      school: {
        location: [],
        systems: [],
        communities: [],
      },
    };
  }

  componentDidMount() {
    Api.get(`schools/${this.state.id}`)
      .then(school => this.setState({ school }));
  }

  render() {
    const { t } = this.props;
    return (
      <React.Fragment>
        <PageHeader title={t('school')} />
        <Row>
          <Col>
            <InfoBox
              entity={this.state.school}
              entityName="school"
              showPdfButton={false}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="6" md="3">
            <ValueBox
              title={t('teachers')}
              value={this.state.school.teachers}
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('students')}
              value={this.state.school.students}
              color="community"
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('haveSystem')}
              value={t(this.state.school.haveSystem)}
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('sufficientWashingFacilities')}
              value={t(this.state.school.sufficientWashingFacilities)}
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="3">
            <SchoolScoreTree
              ecsEsc={this.state.school.ecsEsc}
              ecsEagi={this.state.school.ecsEagi}
              ecsShei={this.state.school.ecsShei}
            />
          </Col>
          <Col sm="12" md="6" xl="3">
            <Picture imgUrl={this.state.school.pictureUrl} />
          </Col>
          <Col sm="12" md="12" xl="6" className="d-flex">
            <PointMap
              latitude={this.state.school.latitude}
              longitude={this.state.school.longitude}
              score={this.state.school.score}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="6">
            <Card className="join-list font-sm">
              <h5>{t('community')}</h5>
              <CardBody className="p-0">
                <Table size="sm" className="table table-striped table-borderless" responsive>
                  <thead>
                    <tr>
                      <th className="col-md-8 col-sm-8">{t('name')}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr key={this.state.school.communities.siasarId}>
                      <td className="filterable-cell col-md-8">
                        <span
                          className={`circle-score background-${(this.state.school.communities.score)}`}
                        />
                        <Link to={`/communities/${this.state.school.communities.siasarId}`}>
                          { this.state.school.communities.name }
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
          <Col sm="12" md="6" xl="6">
            <Card className="join-list font-sm">
              <h5>{t('system')}</h5>
              <CardBody className="p-0">
                <Table size="sm" className="table table-striped table-borderless" responsive>
                  <thead>
                    <tr>
                      <th className="col-md-8 col-sm-8">{t('name')}</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.school.systems && (
                      <tr key={this.state.school.systems.siasarId}>
                        <td className="filterable-cell col-md-8">
                          <span
                            className={`circle-score background-${(this.state.school.systems.score)}`}
                          />
                          <Link to={`/systems/${this.state.school.systems.siasarId}`}>
                            { this.state.school.systems.name }
                          </Link>
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <section className="page-break">
          <SectionHeader title={t('specificData')} />
          <Row>
            <Col md="12" lg="6">
              <CentersList
                title={t('students')}
                items={[
                  {
                    title: `${t('femaleStudents')}`,
                    data: this.state.school.femaleStudents,
                  },
                  {
                    title: `${t('maleStudents')}`,
                    data: this.state.school.maleStudents,
                  },
                ]}
              />
            </Col>
            <Col md="12" lg="6">
              <CentersList
                title={t('water')}
                items={[
                  {
                    title: `${t('system')}`,
                    data: `${t(this.state.school.system)}`,
                  },
                ]}
              />
            </Col>
          </Row>
          <Row>
            <Col md="12" lg="6">
              <CentersList
                title={t('sanitation')}
                items={[
                  {
                    title: `${t('improvedFacilitiesTypeI')}`,
                    data: this.state.school.improvedFacilitiesTypeI,
                  },
                  {
                    title: `${t('improvedFacilitiesTypeIi')}`,
                    data: this.state.school.improvedFacilitiesTypeIi,
                  },
                  {
                    title: `${t('separateFacilitiesStaffStudents')}`,
                    data: `${t(this.state.school.separateFacilitiesStaffStudents)}`,
                  },
                  {
                    title: `${t('separateFacilitiesBoysGirls')}`,
                    data: `${t(this.state.school.separateFacilitiesBoysGirls)}`,
                  },
                ]}
              />
            </Col>
            <Col md="12" lg="6">
              <CentersList
                title={t('hygiene')}
                items={[
                  {
                    title: `${t('installationsWaterSoap')}`,
                    data: this.state.school.installationsWaterSoap,
                  },
                  {
                    title: `${t('separateFacilitiesStaffStudentsHygiene')}`,
                    data: `${t(this.state.school.separateFacilitiesStaffStudentsHygiene)}`,
                  },
                ]}
              />
            </Col>
          </Row>
        </section>
      </React.Fragment>
    );
  }
}

School.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }).isRequired,
  t: PropTypes.func.isRequired,
};

export default translate()(School);
