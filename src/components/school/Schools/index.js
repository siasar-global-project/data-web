import React, { Component } from 'react';
import { entityColors } from 'lib/Utils';
import Entities from 'components/common/Entities';

class Schools extends Component {
  render() {
    return (
      <Entities
        entity="school"
        endpoint="schools"
        indicator="ecsEsc"
        stats
        backgroundColor={entityColors[0]}
      />
    );
  }
}


export default Schools;
