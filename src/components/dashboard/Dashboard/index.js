import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Alert } from 'reactstrap';
import { translate } from 'react-i18next';
import { faHome, faTint, faWrench, faClipboardCheck, faInfoCircle, faSchool, faHospital }
  from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { defaults as chartDefaults } from 'react-chartjs-2';
import moment from 'moment';

import { entityColors, scoreColors, grayColors, formatFloat } from 'lib/Utils';
import Api from 'lib/Api';

import PageHeader from 'components/common/PageHeader';
import SectionHeader from 'components/common/SectionHeader';
import ValueBox from 'components/common/ValueBox';
import BarChart from 'components/common/charts/BarChart';
import LineChart from 'components/common/charts/LineChart';
import PieChart from 'components/common/charts/PieChart';
import GeoJSONMap from 'components/common/maps/GeoJSONMap';
import CountryList from 'components/common/CountryList';

import './style.css';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    const monthsRange = 6;

    this.state = {
      monthsRange,
      stats: {},
      gatheredChartData: null,
      accumulatedChartData: null,
      scoreChartData: null,
      womenChartData: null,
      waterCoverageChartData: null,
      country: null,
      countries: [],
      geoCountries: null,
    };

    this.countryChange = this.countryChange.bind(this);
  }

  componentDidMount() {
    localStorage.setItem('country', null);
    Api.get('communities', { attributes: ['adm0'], group: ['adm0'], order: ['adm0'] })
      .then(countries => this.setState({ countries }));

    Api.get('countries').then(geoCountries => this.setState({ geoCountries }));
  }

  update() {
    const statsParams = { where: {} };
    if (this.state.country) statsParams.where.country = this.state.country.code;

    Api.get('stats', statsParams).then(stats => this.setState({ stats }));

    const limitDate = moment().subtract(this.state.monthsRange - 1, 'months').startOf('month').toISOString();

    const byMonthParams = {
      where: { surveyDate: { $between: [limitDate, moment().toISOString()] } },
    };
    if (this.state.country) byMonthParams.where.country = this.state.country.code;

    Promise.all([
      Api.get('communities/by-month', byMonthParams),
      Api.get('systems/by-month', byMonthParams),
      Api.get('service-providers/by-month', byMonthParams),
      Api.get('technical-providers/by-month', byMonthParams),
    ]).then((results) => {
      const data = results.map(item => item.map(e => ({ x: e.date, y: e.count })));

      this.setState({
        gatheredChartData: [
          { label: this.props.t('community_plural'), data: data[0] },
          { label: this.props.t('system_plural'), data: data[1] },
          { label: this.props.t('serviceProvider_plural'), data: data[2] },
          { label: this.props.t('technicalProvider_plural'), data: data[3] },
        ],
      });
    });

    byMonthParams.accumulated = true;

    Promise.all([
      Api.get('communities/by-month', byMonthParams),
      Api.get('systems/by-month', byMonthParams),
      Api.get('service-providers/by-month', byMonthParams),
      Api.get('technical-providers/by-month', byMonthParams),
    ]).then((results) => {
      const data = results.map(item => item.map(e => ({ x: e.date, y: e.count })));

      this.setState({
        accumulatedChartData: [
          { label: this.props.t('community_plural'), data: data[0] },
          { label: this.props.t('system_plural'), data: data[1] },
          { label: this.props.t('serviceProvider_plural'), data: data[2] },
          { label: this.props.t('technicalProvider_plural'), data: data[3] },
        ],
      });
    });

    const byScoreParams = { where: { score: { $ne: 'N' } }, percentage: true };
    if (this.state.country) byScoreParams.where.country = this.state.country.code;

    Promise.all([
      Api.get('communities/by-score', byScoreParams),
      Api.get('systems/by-score', byScoreParams),
      Api.get('service-providers/by-score', byScoreParams),
      Api.get('technical-providers/by-score', byScoreParams),
    ]).then((results) => {
      const data = results.map(item => item.map(e => ({ x: e.score, y: formatFloat(e.count) })));

      this.setState({
        scoreChartData: [
          { label: 'A', data: [data[0][0], data[1][0], data[2][0], data[3][0]] },
          { label: 'B', data: [data[0][1], data[1][1], data[2][1], data[3][1]] },
          { label: 'C', data: [data[0][2], data[1][2], data[2][2], data[3][2]] },
          { label: 'D', data: [data[0][3], data[1][3], data[2][3], data[3][3]] },
        ],
      });
    });

    const womenParams = { where: { score: { $ne: 'N' }, siasarVersion: 2 }, topLimit: 3, percentage: true };
    if (this.state.country) womenParams.where.country = this.state.country.code;

    Api.get('service-providers/women-scores', womenParams).then((result) => {
      const data = result.map(item => (
        [item.scores.A, item.scores.B, item.scores.C, item.scores.D].map(value => formatFloat(value))
      ));

      this.setState({
        womenChartData: [
          { label: '0', data: data[0], backgroundColor: scoreColors(0.35) },
          { label: '1', data: data[1], backgroundColor: scoreColors(0.65) },
          { label: '2', data: data[2], backgroundColor: scoreColors(0.85) },
          { label: '3+', data: data[3], backgroundColor: scoreColors() },
        ],
      });
    });

    const legalStatusParams = { where: { score: { $ne: 'N' } }, percentage: true };
    if (this.state.country) legalStatusParams.where.country = this.state.country.code;

    Api.get('service-providers/legal-status-scores', legalStatusParams).then((result) => {
      const data = [
        [result.notLegalized.A, result.notLegalized.B, result.notLegalized.C, result.notLegalized.D],
        [result.legalized.A, result.legalized.B, result.legalized.C, result.legalized.D],
      ].map(item => item.map(value => formatFloat(value)));

      this.setState({
        legalStatusChartData: [
          { label: this.props.t('notLegalized'), data: data[0], backgroundColor: scoreColors(0.35) },
          { label: this.props.t('legalized'), data: data[1], backgroundColor: scoreColors() },
        ],
      });
    });

    const waterCoverageParams = {
      where: {},
    };
    if (this.state.country) waterCoverageParams.where.country = this.state.country.code;
    waterCoverageParams.where.siasarVersion = '2';
    Api.get('communities/water-coverage', waterCoverageParams).then((result) => {
      const data = [formatFloat(result.wco * 100)];
      data[1] = formatFloat(100 - data[0]);

      this.setState({
        waterCoverageChartData: [{ data, backgroundColor: [scoreColors()[0], scoreColors()[3]] }],
      });
    });
  }

  countryChange(country) {
    this.setState({ country }, this.update);
  }

  countryMapStyle() {
    return {
      stroke: true,
      color: '#096396',
      opacity: 0.3,
      weight: 3,
      fillColor: '#096396',
      fillOpacity: 0.7,
    };
  }

  render() {
    const { t } = this.props;
    return (
      <React.Fragment>
        <PageHeader>
          <h5 className="mr-2">Dashboard {this.state.country ? this.state.country.name : null}</h5>
          <CountryList all onChange={this.countryChange} />
        </PageHeader>
        {this.state.country ?
          <Alert color="info" className="d-flex align-items-center justify-content-between">
            <div>{this.state.country.name}</div>
          </Alert> : null}
        <GeoJSONMap
          mapCenter={[10, -15]}
          zoomLevel={1}
          data={this.state.geoCountries}
          mapStyle={this.countryMapStyle}
          popupProperty="name"
          title={t('siasarMembers')}
          sticky
        />
        <Row>
          <Col xs="12" sm="6" md="4" lg="4">
            <ValueBox
              title={t('community_plural')}
              value={this.state.stats.communities}
              className="value-box-community"
              icon={faHome}
              href="/communities"
            />
          </Col>
          <Col xs="12" sm="6" md="4" lg="4">
            <ValueBox
              title={t('system_plural')}
              value={this.state.stats.systems}
              className="value-box-system"
              icon={faTint}
              href="/systems"
            />
          </Col>
          <Col xs="12" sm="6" md="4" lg="4">
            <ValueBox
              title={t('serviceProvider_plural')}
              value={this.state.stats.serviceProviders}
              className="value-box-service-provider"
              icon={faWrench}
              href="/service-providers"
            />
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="6" md="4" lg="4">
            <ValueBox
              title={t('technicalProvider_plural')}
              value={this.state.stats.technicalProviders}
              className="value-box-technical-provider"
              icon={faClipboardCheck}
              href="/technical-providers"
            />
          </Col>
          <Col xs="12" sm="6" md="4" lg="4">
            <ValueBox
              title={t('schools')}
              value={this.state.stats.schools}
              className="value-box-school"
              icon={faSchool}
              href="/schools"
            />
          </Col>
          <Col xs="12" sm="6" md="4" lg="4">
            <ValueBox
              title={t('healthCenters')}
              value={this.state.stats.healthCenters}
              className="value-box-health-center"
              icon={faHospital}
              href="/health-centers"
            />
          </Col>
        </Row>
        <section>
          <SectionHeader title={t('generalInformation')} />
          <Row>
            <Col xs="12" sm="6" md="4" lg="3">
              <ValueBox title={t('population')} value={this.state.stats.population} />
            </Col>
            <Col xs="12" sm="6" md="4" lg="3">
              <ValueBox title={t('households')} value={this.state.stats.households} />
            </Col>
            <Col xs="12" sm="6" md="4" lg="3">
              <ValueBox title={t('servedHouseholds')} value={this.state.stats.servedHouseholds} />
            </Col>
            <Col xs="12" sm="6" md="4" lg="3">
              <ValueBox title={t('householdsWithoutWater')} value={this.state.stats.householdsWithoutWater} />
            </Col>
          </Row>
          <Row>
            <Col md="12" xl="6">
              <BarChart
                title={t('gatheredData')}
                subtitle={t('lastMonths', { number: this.state.monthsRange })}
                data={{ datasets: this.state.gatheredChartData }}
                colors={entityColors}
                legend={{ display: false }}
                options={{
                  scales: {
                    xAxes: [{
                      type: 'time',
                      time: {
                        displayFormats: {
                          month: 'YYYY-MM',
                        },
                        tooltipFormat: 'YYYY-MM',
                        round: true,
                        minUnit: 'month',
                      },
                      offset: true,
                    }],
                    yAxes: [{ ticks: { min: 0 } }],
                  },
                  tooltips: {
                    callbacks: {
                      label: item => `${[
                        t('community_plural'),
                        t('system_plural'),
                        t('serviceProvider_plural'),
                        t('technicalProvider_plural'),
                      ][item.datasetIndex]}: ${item.yLabel}`,
                    },
                  },
                }}
              />
            </Col>
            <Col md="12" xl="6">
              <LineChart
                title={t('accumulatedData')}
                subtitle={t('lastMonths', { number: this.state.monthsRange })}
                data={{ datasets: this.state.accumulatedChartData }}
                colors={entityColors}
                legend={{ display: false }}
                options={{
                  scales: {
                    xAxes: [{
                      type: 'time',
                      time: {
                        displayFormats: {
                          month: 'YYYY-MM',
                        },
                        tooltipFormat: 'YYYY-MM',
                        round: true,
                        minUnit: 'month',
                      },
                    }],
                    yAxes: [{ ticks: { min: 0 } }],
                  },
                  tooltips: {
                    callbacks: {
                      label: item => `${[
                        t('community_plural'),
                        t('system_plural'),
                        t('serviceProvider_plural'),
                        t('technicalProvider_plural'),
                      ][item.datasetIndex]}: ${item.yLabel}`,
                      labelColor: item => ({
                        borderColor: 'transparent',
                        backgroundColor: entityColors[item.datasetIndex],
                      }),
                    },
                  },
                }}
              />
            </Col>
          </Row>
          {!this.state.country && this.state.countries.length &&
            <Alert color="info">
              <FontAwesomeIcon icon={faInfoCircle} /> {t('dataSource')}
              <i> {this.state.countries.map(country => country.adm0).join(', ')}</i>
            </Alert>
          }
        </section>
        <section className="bg-light">
          <SectionHeader title={t('dataAnalysis')} />
          <Row>
            <Col md="12" xl="6">
              <PieChart
                title={t('waterCoverage')}
                subtitle={`${t('weightedAverage')} ${t('by')} ${t('population')}`}
                data={{
                  labels: [t('access'), `${t('no')} ${t('access')}`],
                  datasets: this.state.waterCoverageChartData,
                }}
              />
            </Col>
            <Col md="12" xl="6">
              <BarChart
                title={`${t('siasarEntitiesByScore')}`}
                subtitle={t('percentageOverTotal')}
                data={{
                  labels: [t('com'), t('sys'), t('sep'), t('tap')],
                  datasets: this.state.scoreChartData,
                }}
                colors={scoreColors()}
                options={{
                  scales: {
                    xAxes: [{ stacked: true }],
                    yAxes: [{ stacked: true, ticks: { max: 100 } }],
                  },
                }}
              />
            </Col>
            <Col md="12" xl="6">
              <BarChart
                title={`${t('impactLegalizationServiceProvider')}`}
                subtitle={t('percentageOverTotal')}
                data={{
                  labels: ['A', 'B', 'C', 'D'],
                  datasets: this.state.legalStatusChartData,
                }}
                colors={[grayColors[0], grayColors[3]]}
                legend={{
                  labels: {
                    generateLabels: (chart) => {
                      const labels = chartDefaults.global.legend.labels.generateLabels(chart);
                      labels.forEach((item, index) => {
                        item.fillStyle = grayColors[index === 0 ? 0 : 3];
                        item.text = index === 0 ? t('notLegalized') : t('legalized');
                      });
                      return labels;
                    },
                  },
                }}
                options={{
                  scales: {
                    xAxes: [{ stacked: true }],
                    yAxes: [{ stacked: true, ticks: { max: 100 } }],
                  },
                  tooltips: {
                    callbacks: {
                      label: item => `${[
                        t('notLegalized'),
                        t('legalized'),
                      ][item.datasetIndex]}: ${item.yLabel}`,
                    },
                  },
                }}
              />
            </Col>
            <Col md="12" xl="6">
              <BarChart
                title={t('impactFemininePresenceService')}
                subtitle={t('serviceProvideScoreWomanBoard')}
                data={{
                  labels: ['A', 'B', 'C', 'D'],
                  datasets: this.state.womenChartData,
                }}
                colors={grayColors}
                legend={{
                  labels: {
                    generateLabels: (chart) => {
                      const labels = chartDefaults.global.legend.labels.generateLabels(chart);
                      labels.forEach((item, index) => {
                        item.fillStyle = grayColors[index];
                      });
                      return labels;
                    },
                  },
                }}
                options={{
                  scales: {
                    xAxes: [{ stacked: true }],
                    yAxes: [{ stacked: true, ticks: { max: 100 } }],
                  },
                }}
              />
            </Col>
          </Row>
        </section>
      </React.Fragment>
    );
  }
}

Dashboard.propTypes = {
  t: PropTypes.func.isRequired,
};

export default translate()(Dashboard);
