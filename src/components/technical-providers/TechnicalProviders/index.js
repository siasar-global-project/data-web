import React, { Component } from 'react';

import Entities from 'components/common/Entities';

class TechnicalProviders extends Component {
  render() {
    return (
      <Entities
        entity="technicalProvider"
        endpoint="technical-providers"
        indicator="tap"
      />
    );
  }
}

export default TechnicalProviders;
