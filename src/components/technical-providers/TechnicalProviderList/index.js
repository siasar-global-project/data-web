import React, { Component } from 'react';

import EntityList from 'components/common/EntityList';

class TechnicalProviderList extends Component {
  render() {
    return (
      <EntityList
        entity="technicalProvider"
        endpoint="technical-providers"
        fields={['servedHouseholds', 'tap']}
      />
    );
  }
}

export default TechnicalProviderList;
