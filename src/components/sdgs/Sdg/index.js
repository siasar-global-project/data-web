/* eslint no-underscore-dangle: ["error", { "allow": ["_model", "_meta"] }] */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardTitle, CardSubtitle, Row, Col } from 'reactstrap';
import { translate } from 'react-i18next';
import BarChart from 'components/common/charts/BarChart';
import SectionHeader from 'components/common/SectionHeader';
import GeoJSONMap from 'components/common/maps/GeoJSONMap';

import { scoreColors, sdgsColors } from 'lib/Utils';

import Api from 'lib/Api';
import isoSdg from 'assets/images/isoSdg61.png';
import './style.css';

class Sdg extends Component {
  constructor(props) {
    super(props);

    this.state = {
      country: null,
      geoCountries: null,
      sdgsChartLabels: [],
      sdgsChartData: [],
      sdgsCountryChartData: null,
      sdgsCountryChartLabels: null,
      legendGrades: [0, 25, 50, 75, 100],
      legendLabel: ['#FFFFFF', '#b6c1e8', '#86a5dd', '#418cd2'],
      labels: ['unimproved', 'limited', 'basic', 'managedSafely'],
    };

    this.countryChange = this.countryChange.bind(this);
  }

  componentDidMount() {
    const query = { percentage: true };
    Api.get('communities/sdgs', query).then((results) => {
      const labels = Object.keys(results).map(key => results[key].country);
      const dataUnimproved = Object.keys(results).map(key => results[key].unimproved);
      const dataLimited = Object.keys(results).map(key => results[key].limited);
      const dataBasic = Object.keys(results).map(key => results[key].basic);
      const dataManagedSafely = Object.keys(results).map(key => results[key].managedSafely);
      this.setState({
        sdgsChartData: this.graphData(dataUnimproved, dataLimited, dataBasic, dataManagedSafely),
        sdgsChartLabels: labels,
      });
    });

    Api.get('countries').then((geoCountries) => {
      geoCountries.features.map(async (countries) => {
        const iso = countries.properties.iso_a2;
        await Api.get('communities/sdgs', { where: { country: iso } }).then((result) => {
          if (iso.search('PE|PY|CR|BR|UG|TZ') === 0) {
            countries.properties.color = this.getColor(null);
            return countries;
          }
          countries.properties.wco = Math.round(result[0].wco * 100);
          countries.properties.color = this.getColor(Math.round(result[0].wco * 100));
          return countries;
        });
        this.setState({ geoCountries });
      });
    });
    this.countryChange('NI');
  }

  getColor(wco) {
    switch (true) {
      case (wco >= 75):
        return '#418cd2';
      case (wco >= 50):
        return '#86a5dd';
      case (wco >= 25):
        return '#b6c1e8';
      case (wco != null):
        return '#FFFFFF';
      default:
        return '#FAFAF8';
    }
  }

  countryChange(country) {
    this.setState({ country }, this.update);
  }

  update() {
    const query = { where: {}, percentage: true };
    if (this.state.country) query.where.country = this.state.country;
    Api.get('communities/sdgs', query).then((results) => {
      const labels = Object.keys(results).map(key => results[key].country);
      const dataUnimproved = Object.keys(results).map(key => results[key].unimproved);
      const dataLimited = Object.keys(results).map(key => results[key].limited);
      const dataBasic = Object.keys(results).map(key => results[key].basic);
      const dataManagedSafely = Object.keys(results).map(key => results[key].managedSafely);
      this.setState({
        sdgsCountryChartData: this.graphData(dataUnimproved, dataLimited, dataBasic, dataManagedSafely),
        sdgsCountryChartLabels: labels,
      });
    });
  }


  countryMapStyle(feature) {
    return {
      stroke: true,
      fillColor: feature.properties.color,
      weight: 1,
      opacity: 1,
      color: '#FFFFFF',
      fillOpacity: 0.7,
    };
  }

  graphData(dataUnimproved, dataLimited, dataBasic, dataManagedSafely) {
    return [
      {
        label: this.state.labels[0],
        data: dataUnimproved,
      },
      {
        label: this.state.labels[1],
        data: dataLimited,
      },
      {
        label: this.state.labels[2],
        data: dataBasic,
      },
      {
        label: this.state.labels[3],
        data: dataManagedSafely,
      },
    ];
  }

  render() {
    const { t } = this.props;
    let datasets = [];
    let datasetsByCountry = [];
    if (this.state.sdgsChartData && this.state.sdgsChartData.length) {
      datasets = this.state.sdgsChartData.map((value, key) => ({
        label: t(value.label),
        data: value.data,
        backgroundColor: sdgsColors[key],
        hoverBackgroundColor: sdgsColors[key],
      }));
    }
    if (this.state.sdgsCountryChartData && this.state.sdgsCountryChartData.length) {
      datasetsByCountry = this.state.sdgsCountryChartData.map((value, key) => ({
        label: t(value.label),
        data: value.data,
        backgroundColor: sdgsColors[key],
        hoverBackgroundColor: sdgsColors[key],
      }));
    }
    return (
      <React.Fragment>
        <SectionHeader />
        <h3 className="text-primary sdgTitle">{t('sdgTitle')}</h3>
        <Row>
          <Col sm="8" className="text-primary text-justify">
            <h3 className="text-primary"><b>{t('sdgSubtitle')}</b></h3>
            {t('sdgBody')}
          </Col>
          <Col sm="4" className="text-center">
            <img src={isoSdg} alt="SIASAR" className="rounded" />
          </Col>
        </Row>
        <section className="bg-light">
          <Row>
            <Col md="12" xl="6">
              <BarChart
                title={`${t('potableWaterService')}`}
                subtitle={`${t('by')} ${t('country')}`}
                data={{
                  labels: this.state.sdgsChartLabels,
                  datasets: (datasets && datasets.length > 0) ? datasets : null,
                  legend: { display: true },
                }}
                colors={scoreColors()}
                options={{
                  percentage: true,
                  scales: {
                    xAxes: [{ stacked: true }],
                    yAxes: [{
                      stacked: true,
                      ticks: {
                        max: 100,
                        callback: value => `${value} %`,
                      },
                    }],
                  },
                  onClick: (e, item) => {
                    if (Object.keys(item).length !== 0) {
                      this.countryChange(item[0]._model.label);
                    }
                  },
                  tooltips: {
                    callbacks: {
                      label: (tooltipItem, data) => {
                        const dataset = data.datasets[tooltipItem.datasetIndex];
                        const currentValue = dataset.data[tooltipItem.index];
                        return `${currentValue} %`;
                      },
                    },
                  },
                }}
              />
            </Col>
            <Col md="12" xl="6">
              <BarChart
                title={`${t('detail')} ${t('potableWaterService')}: `}
                subtitle={`${t(`countries.${this.state.country}`)}`}
                data={{
                  labels: this.state.sdgsCountryChartLabels,
                  datasets: (datasetsByCountry && datasetsByCountry.length > 0) ? datasetsByCountry : null,
                  legend: { display: true },
                }}
                colors={scoreColors()}
                options={{
                  percentage: true,
                  scales: {
                    yAxes: [{
                      ticks: {
                        callback: value => `${value} %`,
                      },
                    }],
                  },
                  tooltips: {
                    callbacks: {
                      label: (tooltipItem, data) => {
                        const dataset = data.datasets[tooltipItem.datasetIndex];
                        const currentValue = dataset.data[tooltipItem.index];
                        return `${currentValue} %`;
                      },
                    },
                  },
                }}
              />
            </Col>
          </Row>
          <Row className="descriptions ml-1 mr-1 sidebar-nav">
            <Col xs="2" xl="2" style={{ backgroundColor: sdgsColors[3] }} />
            <Col xs="10" xl="10">
              <Card>
                <CardTitle>{t('managedSafely').toUpperCase()}</CardTitle>
                <CardSubtitle>{t('managedSafelyDescription')}</CardSubtitle>
              </Card>
            </Col>
            <Col xs="2" xl="2" style={{ backgroundColor: sdgsColors[2] }} />
            <Col xs="10" xl="10">
              <Card>
                <CardTitle>{t('basic').toUpperCase()}</CardTitle>
                <CardSubtitle>{t('basicDescription')}</CardSubtitle>
              </Card>
            </Col>
            <Col xs="2" xl="2" style={{ backgroundColor: sdgsColors[1] }} />
            <Col xs="10" xl="10">
              <Card>
                <CardTitle>{t('limited').toUpperCase()}</CardTitle>
                <CardSubtitle>{t('limitedDescription')}</CardSubtitle>
              </Card>
            </Col>
            <Col xs="2" xl="2" style={{ backgroundColor: sdgsColors[0] }} />
            <Col xs="10" xl="10">
              <Card>
                <CardTitle>{t('unimproved').toUpperCase()}</CardTitle>
                <CardSubtitle>{t('unimprovedDescription')}</CardSubtitle>
              </Card>
            </Col>
          </Row>
          <Row className="ml-1 mr-1">
            <Col md="12" xl="12">
              <GeoJSONMap
                mapCenter={[10, -15]}
                zoomLevel={1}
                data={this.state.geoCountries}
                mapStyle={this.countryMapStyle}
                popupProperty="name"
                title={t('improvedWaterCoverageMap')}
                sticky
                legendLabel={this.state.legendLabel}
                legendGrades={this.state.legendGrades}
              />
            </Col>
          </Row>
        </section>
      </React.Fragment>
    );
  }
}

Sdg.propTypes = {
  t: PropTypes.func.isRequired,
};

export default translate()(Sdg);
