import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { translate } from 'react-i18next';
import { faHome, faTint, faWrench, faClipboardCheck, faSchool, faHospital }
  from '@fortawesome/free-solid-svg-icons';
import Api from 'lib/Api';

import PageHeader from 'components/common/PageHeader';
import ValueBox from 'components/common/ValueBox';
import GeoJSONMap from 'components/common/maps/GeoJSONMap';
import CountryList from 'components/common/CountryList';

import './style.css';

class PublicDashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stats: {},
      country: null,
      geoCountries: null,
    };

    this.countryChange = this.countryChange.bind(this);
  }

  componentDidMount() {
    localStorage.setItem('country', null);
    Api.get('countries').then(geoCountries => this.setState({ geoCountries }));
  }

  update() {
    const statsParams = { where: {} };
    if (this.state.country) statsParams.where.country = this.state.country.code;
    Api.get('stats', statsParams).then(stats => this.setState({ stats }));
  }

  countryChange(country) {
    this.setState({ country }, this.update);
  }

  countryMapStyle() {
    return {
      stroke: true,
      color: '#096396',
      opacity: 0.3,
      weight: 3,
      fillColor: '#096396',
      fillOpacity: 0.7,
    };
  }

  render() {
    const { t } = this.props;
    return (
      <React.Fragment>
        <PageHeader>
          <h5 className="mr-2">Dashboard {this.state.country ? this.state.country.name : null}</h5>
          <CountryList all onChange={this.countryChange} />
        </PageHeader>
        <GeoJSONMap
          mapCenter={[10, -15]}
          zoomLevel={1}
          data={this.state.geoCountries}
          mapStyle={this.countryMapStyle}
          popupProperty="name"
          title={t('siasarMembers')}
          sticky
        />
        <Row className="d-flex align-items-stretch bd-highlight">
          <Col xs="12" sm="4" md="2" lg="2">
            <ValueBox
              title={t('community_plural')}
              value={this.state.stats.communities}
              className="value-box-community "
              icon={faHome}
              href="/communities"
            />
          </Col>
          <Col xs="12" sm="4" md="2" lg="2">
            <ValueBox
              title={t('system_plural')}
              value={this.state.stats.systems}
              className="value-box-system"
              icon={faTint}
              href="/systems"
            />
          </Col>
          <Col xs="12" sm="4" md="2" lg="2">
            <ValueBox
              title={t('serviceProvider_plural')}
              value={this.state.stats.serviceProviders}
              className="value-box-service-provider"
              icon={faWrench}
              href="/service-providers"
            />
          </Col>
          <Col xs="4" sm="4" md="2" lg="2">
            <ValueBox
              title={t('technicalProvider_plural')}
              value={this.state.stats.technicalProviders}
              className="value-box-technical-provider"
              icon={faClipboardCheck}
              href="/technical-providers"
            />
          </Col>
          <Col xs="4" sm="4" md="2" lg="2">
            <ValueBox
              title={t('schools')}
              value={this.state.stats.schools}
              className="value-box-school"
              icon={faSchool}
              href="/schools"
            />
          </Col>
          <Col xs="4" sm="4" md="2" lg="2">
            <ValueBox
              title={t('healthCenters')}
              value={this.state.stats.healthCenters}
              className="value-box-health-center"
              icon={faHospital}
              href="/health-centers"
            />
          </Col>
        </Row>
        <section>
          <Row>
            <Col xs="12" sm="6" md="4" lg="3">
              <ValueBox title={t('population')} value={this.state.stats.population} />
            </Col>
            <Col xs="12" sm="6" md="4" lg="3">
              <ValueBox title={t('households')} value={this.state.stats.households} />
            </Col>
            <Col xs="12" sm="6" md="4" lg="3">
              <ValueBox title={t('servedHouseholds')} value={this.state.stats.servedHouseholds} />
            </Col>
            <Col xs="12" sm="6" md="4" lg="3">
              <ValueBox title={t('householdsWithoutWater')} value={this.state.stats.householdsWithoutWater} />
            </Col>
          </Row>
        </section>
      </React.Fragment>
    );
  }
}

PublicDashboard.propTypes = {
  t: PropTypes.func.isRequired,
};

export default translate()(PublicDashboard);
