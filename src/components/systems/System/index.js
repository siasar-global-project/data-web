import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Alert } from 'reactstrap';
import { translate } from 'react-i18next';

import Api from 'lib/Api';
import { entityColors } from 'lib/Utils';

import PageHeader from 'components/common/PageHeader';
import ValueBox from 'components/common/ValueBox';
import Picture from 'components/common/Picture';
import PointMap from 'components/common/maps/PointMap';
import SystemScoreTree from 'components/systems/SystemScoreTree';
import JoinList from 'components/common/JoinList';
import InfoBox from 'components/common/InfoBox';
import IndicatorsList from 'components/common/IndicatorsList';

import { faTint } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class System extends Component {
  constructor(props) {
    super(props);

    const { match: { params } } = props;

    this.state = {
      id: parseInt(params.id, 10),
      system: {
        location: [],
        communities: [],
        flow: [],
        supplyType: [],
        serviceProviders: [],
        schools: [],
        healthCenters: [],
      },
    };
  }

  componentDidMount() {
    Api.get(`systems/${this.state.id}`)
      .then(system => this.setState({ system }));
  }

  render() {
    const { t } = this.props;
    const chlora = (this.state.system.chlora) ? t('yes') : t('no');
    return (
      <React.Fragment>
        <PageHeader title={t('system')} />
        <Row>
          <Col>
            <InfoBox
              entity={this.state.system}
              entityName="system"
            />
          </Col>
        </Row>
        <Row>
          <Col sm="6" md="3">
            <ValueBox
              title={t('servedHouseholds')}
              value={this.state.system.servedHouseholds}
              style={{ backgroundColor: entityColors[1] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('buildingYear')}
              value={this.state.system.buildingYear}
              format={false}
              style={{ backgroundColor: entityColors[1] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('flow')}
              value={`${this.state.system.flow.value} ${this.state.system.flow.unit}`}
              style={{ backgroundColor: entityColors[1] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('chlora')}
              value={chlora}
              style={{ backgroundColor: entityColors[1] }}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="12" xl="12">
            <Alert color="info">
              <FontAwesomeIcon icon={faTint} /> <b>{t('supplyType')}: </b>
              <i> {this.state.system.supplyType && this.state.system.supplyType.map(type => t(type)).join(', ')}</i>
            </Alert>
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="3">
            <SystemScoreTree
              wsi={this.state.system.wsi}
              wsiAut={this.state.system.wsiAut}
              wsiInf={this.state.system.wsiInf}
              wsiPro={this.state.system.wsiPro}
              wsiTre={this.state.system.wsiTre}
            />
          </Col>
          <Col sm="12" md="6" xl="3">
            <Picture imgUrl={this.state.system.pictureUrl} />
          </Col>
          <Col sm="12" md="6" xl="3">
            <Picture imgUrl={this.state.system.sketchUrl} />
          </Col>
          <Col sm="12" md="12" xl="3" className="d-flex">
            <PointMap
              latitude={this.state.system.latitude}
              longitude={this.state.system.longitude}
              score={this.state.system.score}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="6">
            <JoinList
              title={t('community_plural')}
              items={this.state.system.communities}
              path="communities"
            />
          </Col>
          <Col sm="12" md="6" xl="6">
            <JoinList
              title={t('serviceProvider_plural')}
              items={this.state.system.serviceProviders}
              path="service-providers"
            />
          </Col>
        </Row>
        <Row>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('autonomy')}
              items={[
                { title: `${t('sufficientVolume')}`, value: this.state.system.wsiAut },
              ]}
            />
          </Col>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('waterProductionInfrastructure')}
              items={[
                {
                  title: `${t('catchmentConditionFactor')}`,
                  value: this.state.system.catf,
                },
                {
                  title: `${t('transmissionCondition')}`,
                  value: this.state.system.traf,
                },
                {
                  title: `${t('storageCondition')}`,
                  value: this.state.system.stof,
                },
                {
                  title: `${t('distributionCondition')}`,
                  value: this.state.system.disf,
                },
              ]}
            />
          </Col>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('treatmentInfraestructure')}
              items={[
                { title: `${t('treatSust')}`, value: this.state.system.treatSust },
                { title: `${t('treatPat')}`, value: this.state.system.treatPat },
              ]}
            />
          </Col>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('catchmentAreaProtection')}
              items={[
                { title: `${t('status')}`, value: this.state.system.wsiPro },
              ]}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

System.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }).isRequired,
  t: PropTypes.func.isRequired,
};

export default translate()(System);
