import React, { Component } from 'react';

import EntityList from 'components/common/EntityList';

class ServiceProviderList extends Component {
  render() {
    return (
      <EntityList
        entity="serviceProvider"
        endpoint="service-providers"
        fields={['servedHouseholds', 'sep']}
      />
    );
  }
}

export default ServiceProviderList;
