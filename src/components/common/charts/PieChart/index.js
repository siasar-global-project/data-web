import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Doughnut } from 'react-chartjs-2';

import Chart from 'components/common/charts/Chart';

class PieChart extends Component {
  render() {
    return (
      <Chart title={this.props.title} subtitle={this.props.subtitle} data={this.props.data}>
        <Doughnut
          data={this.props.data}
          options={{
            cutoutPercentage: 0,
            ...this.props.options,
          }}
          legend={{
            display: true,
            position: 'bottom',
            fullWidth: true,
            ...this.props.legend,
          }}
        />
      </Chart>
    );
  }
}

PieChart.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.object,
  options: PropTypes.object,
  subtitle: PropTypes.string,
  legend: PropTypes.object,
};

PieChart.defaultProps = {
  options: {},
  subtitle: null,
  data: null,
  legend: {},
};

export default PieChart;
