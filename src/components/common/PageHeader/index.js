import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './style.css';

class PageHeader extends Component {
  render() {
    return (
      <div
        className={
          `page-header
          p-3 bg-primary
          text-secondary
          d-flex
          justify-content-end
          align-items-center
          ${this.props.className}`
        }
      >
        { this.props.title ? (
          <h5 className="mb-0">{this.props.title}</h5>
        ) : this.props.children }
      </div>
    );
  }
}

PageHeader.propTypes = {
  title: PropTypes.string,
  children: PropTypes.arrayOf(PropTypes.element),
  className: PropTypes.string,
};

PageHeader.defaultProps = {
  title: null,
  children: null,
  className: null,
};

export default PageHeader;
