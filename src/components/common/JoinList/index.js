import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Card, CardBody } from 'reactstrap';
import { translate } from 'react-i18next';

import './style.css';

class JoinList extends Component {
  render() {
    const { t } = this.props;
    return (
      <Card className={`join-list ${this.props.className} font-sm`}>
        {this.props.title &&
          <h5>{this.props.title}</h5>
        }
        <CardBody className="p-0">
          <Table size="sm" className="table table-striped table-borderless" responsive>
            <thead>
              <tr>
                <th className="col-md-8 col-sm-8">{t('name')}</th>
                <th className="col-md-4 col-sm-4">{t('servedHouseholds')}</th>
              </tr>
            </thead>
            <tbody>
              {this.props.items.map(item => (
                <tr key={item.siasarId}>
                  <td className="filterable-cell col-md-8">
                    <span
                      className={`circle-score background-${(item.score)}`}
                    />
                    <Link to={`/${this.props.path}/${item.siasarId}`}>{ item.name }</Link>
                  </td>
                  <td className="col-md-4">{item.join.servedHouseholds}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    );
  }
}

JoinList.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  path: PropTypes.string.isRequired,
  className: PropTypes.string,
  t: PropTypes.func.isRequired,
};

JoinList.defaultProps = {
  title: null,
  className: null,
};

export default translate()(JoinList);
