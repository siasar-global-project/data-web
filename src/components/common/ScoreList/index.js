import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ButtonDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';
import { translate } from 'react-i18next';

class ScoreList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scores: ['A', 'B', 'C', 'D'],
      isOpen: false,
    };

    this.toggle = this.toggle.bind(this);
    this.update = this.update.bind(this);
  }

  update(event) {
    this.props.onChange(
      event.target.dataset.value,
      this.state.scores,
    );
  }

  toggle() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    const { t } = this.props;
    return (
      <ButtonDropdown size="sm" isOpen={this.state.isOpen} toggle={this.toggle} className={this.props.className}>
        <DropdownToggle caret color="white">{t('score')}</DropdownToggle>
        <DropdownMenu>
          {this.props.all &&
            <DropdownItem data-value="" onClick={this.update}>{t('all')}</DropdownItem>
          }
          {this.state.scores.map(score => (
            <DropdownItem
              key={score}
              data-value={score}
              onClick={this.update}
            >{score}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}

ScoreList.propTypes = {
  all: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  t: PropTypes.func.isRequired,
};

ScoreList.defaultProps = {
  all: false,
  className: null,
};

export default translate()(ScoreList);
