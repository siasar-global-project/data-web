import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Row, Col, Alert } from 'reactstrap';
import { translate } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf, faHome, faCalendarAlt, faGlobe } from '@fortawesome/free-solid-svg-icons';

import config from 'config';
import './style.css';

class InfoBox extends Component {
  render() {
    const { t } = this.props;
    const reportLink = `${config.pentaho.baseUrl}${config.pentaho.reports[`${this.props.entityName}`]}/report?userid=${config.pentaho.username}&password=${config.pentaho.password}&id=${this.props.entity.siasarId}`; // eslint-disable-line max-len
    return (
      <Alert color="info" className="info-box">
        {this.props.showPdfButton && (
          <Button
            color="dark"
            href={reportLink}
            target="_blank"
            className="d-print-none info-box-button"
            title={t('reportLink')}
          >
            <FontAwesomeIcon icon={faFilePdf} fixedWidth />
          </Button>
        )}
        <Row>
          <Col>
            <h3><strong>{this.props.entity.name}</strong></h3>
          </Col>
        </Row>
        <Row>
          <Col>
            <FontAwesomeIcon icon={faHome} fixedWidth />
            <strong> ID #: </strong>{this.props.entity.siasarId}
          </Col>
        </Row>
        <Row>
          <Col>
            <FontAwesomeIcon icon={faCalendarAlt} fixedWidth />
            <strong> {t('date')}: </strong>{this.props.entity.surveyDate}
          </Col>
        </Row>
        <Row>
          <Col>
            <FontAwesomeIcon icon={faGlobe} fixedWidth />
            <strong> {t('location')}: </strong>
            {`${this.props.entity.adm0}, ${this.props.entity.location.filter(a => a).join(', ')}`}
          </Col>
        </Row>
      </Alert>
    );
  }
}

InfoBox.propTypes = {
  entity: PropTypes.object,
  entityName: PropTypes.string,
  showPdfButton: PropTypes.bool,
  t: PropTypes.func.isRequired,
};

InfoBox.defaultProps = {
  entity: [],
  showPdfButton: true,
  entityName: null,
};

export default translate()(InfoBox);
