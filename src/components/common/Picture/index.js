import React, { Component } from 'react';
import PropTypes from 'prop-types';

import placeholder from 'assets/images/placeholder.png';

import './style.css';

class Picture extends Component {
  render() {
    return (
      <div className="picture mb-4">
        {this.props.imgUrl ?
          <img src={this.props.imgUrl} alt="Entity" /> :
          <img src={placeholder} alt="Empty" />
        }
      </div>
    );
  }
}

Picture.propTypes = {
  imgUrl: PropTypes.string,
};

Picture.defaultProps = {
  imgUrl: null,
};

export default Picture;
