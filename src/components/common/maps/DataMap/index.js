import React, { Component } from 'react';
import { Map, TileLayer } from 'react-leaflet';
import PropTypes from 'prop-types';
import Legend from 'components/common/maps/LegendMap';

import 'leaflet/dist/leaflet.css';

import spinner from 'assets/images/spinner.gif';

import './style.css';

const stamenTonerTiles = 'https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png';
const stamenTonerAttr = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors' +
  '&copy; <a href="https://carto.com/attributions">CARTO</a>';

class DataMap extends Component {
  render() {
    return (
      <Map
        ref={map => (this.map = map)}
        center={this.props.mapCenter}
        zoom={this.props.zoomLevel}
        className={`data-map ${this.props.className} ${this.props.sticky ? 'sticky-map' : null}`}
        bounds={this.props.bounds}
        whenReady={this.props.whenReady}
        useFlyTo
      >
        {this.props.title ? <h5>{this.props.title}</h5> : null}
        {this.props.loading ? <img src={spinner} className="data-map-loading" alt="loading" /> : null}
        <TileLayer
          ref={layer => (this.baseLayer = layer)}
          attribution={stamenTonerAttr}
          url={stamenTonerTiles}
          onAdd={this.props.baseLayerAdded}
          noWrap
        />
        {this.props.children}
        {this.props.legendLabel ?
          <Legend legendLabel={this.props.legendLabel} legendGrades={this.props.legendGrades} /> : null}
      </Map>
    );
  }
}

DataMap.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  mapCenter: PropTypes.array.isRequired,
  zoomLevel: PropTypes.number.isRequired,
  children: PropTypes.element,
  sticky: PropTypes.bool,
  bounds: PropTypes.any,
  whenReady: PropTypes.func,
  baseLayerAdded: PropTypes.func,
  loading: PropTypes.bool,
  legendLabel: PropTypes.array,
  legendGrades: PropTypes.array,
};

DataMap.defaultProps = {
  title: null,
  className: null,
  sticky: false,
  bounds: null,
  whenReady: null,
  baseLayerAdded: null,
  loading: false,
  children: null,
  legendLabel: null,
  legendGrades: null,
};

export default DataMap;
