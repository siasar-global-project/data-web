import React, { Component } from 'react';
import PropTypes from 'prop-types';
import L from 'leaflet';
import 'leaflet.vectorgrid';

import DataMap from 'components/common/maps/DataMap';
import { scoreColor } from 'lib/Utils';
import Api from 'lib/Api';

class MVTMap extends Component {
  componentDidMount() {
    const params = { where: {} };
    if (this.props.country) params.where.country = this.props.country;

    const vectorTileOptions = {
      rendererFactory: L.canvas.tile,
      vectorTileLayerStyles: {
        point: (properties, zoom) => {
          const radius = Math.max((5 * zoom) / 10, 1);
          return {
            weight: 1,
            color: 'black',
            opacity: 0.5,
            fillColor: scoreColor(properties.score),
            fill: true,
            stroke: zoom > 7,
            radius,
            fillOpacity: 0.8,
          };
        },
      },
      interactive: true,
      noWrap: true,
    };

    this.layer = L.vectorGrid.protobuf(
      Api.buildUrl(`${this.props.endpoint}/tiles/{z}/{x}/{y}`, params),
      vectorTileOptions,
    )
      .on('mouseover', (e) => {
        e.layer.getCenter = () => e.latlng;
        e.target
          .bindTooltip(`${e.layer.properties.siasar_id}: ${e.layer.properties.name}`)
          .openTooltip(e.layer, e.latlng);
      })
      .on('mouseout', (e) => {
        e.target.closeTooltip(e.latlng);
      });

    this.map = this.dataMap.map.leafletElement;
  }

  componentDidUpdate(prevProps) {
    if (this.props.country !== prevProps.country) {
      this.fitBounds();
    }
  }

  fitBounds() {
    const params = { where: {} };
    if (this.props.country) params.where.country = this.props.country;
    Api.get(`${this.props.endpoint}/bounds`, params).then((bounds) => {
      this.map.removeLayer(this.layer);
      this.layer.setUrl(Api.buildUrl(`${this.props.endpoint}/tiles/{z}/{x}/{y}`, params));
      this.map.on('zoomend', () => {
        this.map.off('zoomend');
        this.map.addLayer(this.layer);
      });
      this.map.flyToBounds(bounds);
    });
  }

  baseLayerAdded() {
    this.fitBounds();
  }

  render() {
    return (
      <DataMap
        ref={dataMap => (this.dataMap = dataMap)}
        title={this.props.title}
        className={`mvt-map ${this.props.className}`}
        mapCenter={this.props.mapCenter}
        zoomLevel={this.props.zoomLevel}
        sticky={this.props.sticky}
        baseLayerAdded={() => this.baseLayerAdded()}
      />
    );
  }
}

MVTMap.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  mapCenter: PropTypes.array.isRequired,
  zoomLevel: PropTypes.number.isRequired,
  endpoint: PropTypes.string.isRequired,
  sticky: PropTypes.bool,
  country: PropTypes.string,
};

MVTMap.defaultProps = {
  title: null,
  className: null,
  sticky: false,
  country: null,
};

export default MVTMap;
