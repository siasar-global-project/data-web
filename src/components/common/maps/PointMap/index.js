import React, { Component } from 'react';
import { CircleMarker } from 'react-leaflet';
import PropTypes from 'prop-types';

import { scoreColor } from 'lib/Utils';

import Loading from 'components/common/Loading';
import DataMap from 'components/common/maps/DataMap';

import './style.css';

const zoomLevel = 8;

class PointMap extends Component {
  render() {
    if (!this.props.latitude || !this.props.longitude) {
      return <Loading />;
    }

    const position = [this.props.latitude, this.props.longitude];

    return (
      <DataMap
        className={`point-map ${this.props.className}`}
        mapCenter={position}
        zoomLevel={zoomLevel}
      >
        <CircleMarker
          center={position}
          color="#000"
          opacity={0.3}
          fillColor={scoreColor(this.props.score)}
          fillOpacity={1}
          radius={15}
        />
      </DataMap>
    );
  }
}

PointMap.propTypes = {
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  score: PropTypes.string,
  className: PropTypes.string,
};

PointMap.defaultProps = {
  latitude: null,
  longitude: null,
  score: null,
  className: null,
};

export default PointMap;
