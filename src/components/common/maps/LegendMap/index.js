import { MapControl, withLeaflet } from 'react-leaflet';
import L from 'leaflet';
import './style.css';

class Legend extends MapControl {
  createLeafletElement() {}
  componentWillUnmount() {}
  componentDidMount() {
    const legend = L.control({ position: 'bottomright' });

    legend.onAdd = () => {
      const div = L.DomUtil.create('div', 'info legend');
      const grades = this.props.legendGrades;
      const colors = this.props.legendLabel;
      const labels = [];
      let from;
      let to;
      for (let i = 0; i < grades.length; i += 1) {
        from = grades[i];
        to = grades[i + 1];
        if (to) {
          labels.push(`<i style="background:${colors[i]}"></i>${from}${(to) ? `-${to}` : ' '}`);
        }
      }
      div.innerHTML = labels.join('<br>');
      return div;
    };

    const { map } = this.props.leaflet;
    legend.addTo(map);
  }
}

export default withLeaflet(Legend);
