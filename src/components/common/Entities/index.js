import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Row, Col, Table, Button, Alert } from 'reactstrap';
import { translate } from 'react-i18next';

import { scoreColors, randomColors, camelToDash } from 'lib/Utils';
import Api from 'lib/Api';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileDownload } from '@fortawesome/free-solid-svg-icons';

import PageHeader from 'components/common/PageHeader';
import ValueBox from 'components/common/ValueBox';
import BarChart from 'components/common/charts/BarChart';
import CountryList from 'components/common/CountryList';
import MVTMap from 'components/common/maps/MVTMap';

class Entities extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stats: {},
      scoreChartData: null,
      countryChartData: null,
      lastRecords: [],
      topRecords: [],
      country: null,
    };

    this.countryChange = this.countryChange.bind(this);
    this.exportCSV = this.exportCSV.bind(this);
  }

  update() {
    const query = { where: {} };
    if (this.state.country) query.where.country = this.state.country.code;

    if (this.props.stats) {
      Api.get(`${this.props.endpoint}/stats`, query).then(stats => this.setState({ stats }));
    }

    query.where.score = { $ne: 'N' };
    Api.get(`${this.props.endpoint}/by-score`, query).then(result => this.setState({
      scoreChartData: {
        labels: ['A', 'B', 'C', 'D'],
        datasets: [{ data: result.map(value => value.count) }],
      },
    }));

    query.group = ['country'];
    query.order = ['country'];
    Api.get(`${this.props.endpoint}/count`, query).then(result => this.setState({
      countryChartData: {
        labels: result.map(value => value.country),
        datasets: [{ data: result.map(value => value.count) }],
      },
    }));

    delete query.group;
    query.attributes = ['siasarId', 'name', 'surveyDate', 'country'];
    query.order = [['surveyDate', 'desc']];
    query.limit = 10;
    Api.get(this.props.endpoint, query).then(lastRecords => this.setState({ lastRecords }));

    query.where[this.props.indicator] = { $ne: 'null' };
    query.where.siasarVersion = '2';
    query.attributes = ['siasarId', 'name', 'score', this.props.indicator, 'country'];
    query.order = [[this.props.indicator, 'desc']];
    Api.get(this.props.endpoint, query).then(topRecords => this.setState({ topRecords }));
  }

  countryChange(country) {
    this.setState({ country }, this.update);
    if (this.props.countryChange) this.props.countryChange(country);
  }

  exportCSV() {
    const params = {
      attributes: [],
      where: {},
      order: [['surveyDate', 'desc']],
    };
    if (this.state.country) params.where.country = this.state.country.code;
    params.attributes = {
      exclude: ['surveyYear', 'surveyMonth', 'geom', 'createdAt', 'updatedAt'],
    };
    delete params.limit;
    params.format = 'csv';
    Api.download(this.props.endpoint, params);
  }

  render() {
    const { t } = this.props;
    return (
      <React.Fragment>
        <PageHeader>
          <h5 className="mr-2">
            {`${this.state.country ? `${this.state.country.name} - ` : ''} ${t(`${this.props.entity}_plural`)}`}
          </h5>
          <CountryList
            className="mr-2 d-print-none"
            endpoint={this.props.endpoint}
            all
            country={this.state.country}
            onChange={this.countryChange}
          />
        </PageHeader>
        {this.state.country ?
          <Alert color="info" className="d-flex align-items-center justify-content-between">
            <div>{this.state.country.name}</div>
          </Alert> : null}
        <Row>
          <Col>
            <MVTMap
              ref={map => (this.map = map)}
              mapCenter={[0, 0]}
              zoomLevel={1}
              endpoint={this.props.endpoint}
              country={this.state.country ? this.state.country.code : null}
              sticky
            />
          </Col>
        </Row>
        {this.props.stats &&
          <Row>
            {Object.keys(this.state.stats).map(item => (
              <Col sm="6" md="3" key={`box-${item}`}>
                <ValueBox
                  title={t(item)}
                  value={this.state.stats[item]}
                  className={`value-box-${camelToDash(this.props.entity)}`}
                />
              </Col>
            ))}
          </Row>
        }
        <Row>
          <Col sm="12" md="6" className="mb-2">
            <BarChart
              title={`${t(`${this.props.entity}_plural`)} ${t('by')} ${t('score')}`}
              data={this.state.scoreChartData}
              colors={[scoreColors()]}
              legend={{ display: false }}
            />
          </Col>
          <Col sm="12" md="6" className="mb-2">
            <BarChart
              title={`${t(`${this.props.entity}_plural`)} ${t('by')} ${t('country')}`}
              data={this.state.countryChartData}
              colors={[randomColors]}
              legend={{ display: false }}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" className="mb-2">
            <h5>
              {t('lastRecords')}
              <Link to={`/${this.props.endpoint}/list`} className="font-sm ml-2">{t('showAll')}</Link>
              <div className="float-right">
                <Button color="info" size="sm" onClick={this.exportCSV} title="Download CSV">
                  <FontAwesomeIcon icon={faFileDownload} fixedWidth />
                </Button>
              </div>
            </h5>
            <Table size="sm" responsive className="font-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>{t('name')}</th>
                  <th>{t('date')}</th>
                  <th>{t('country')}</th>
                </tr>
              </thead>
              <tbody>
                {this.state.lastRecords.map(record => (
                  <tr key={`last-${record.siasarId}`}>
                    <td><Link to={`/${this.props.endpoint}/${record.siasarId}`}>{record.siasarId}</Link></td>
                    <td>{record.name}</td>
                    <td className="text-nowrap">{record.surveyDate}</td>
                    <td>{record.country}</td>
                  </tr>
                ))}
              </tbody>
            </Table>

          </Col>
          <Col sm="12" md="6" className="mb-2">
            <h5>
              {t('topRecords')}
              <Link to={`/${this.props.endpoint}/list`} className="font-sm ml-2">{t('showAll')}</Link>
            </h5>
            <Table size="sm" responsive className="font-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>{t('name')}</th>
                  <th>{t('score')}</th>
                  <th>{t(this.props.indicator)}</th>
                  <th>{t('country')}</th>
                </tr>
              </thead>
              <tbody>
                {this.state.topRecords.map(record => (
                  <tr key={`last-${record.siasarId}`}>
                    <td><Link to={`/${this.props.endpoint}/${record.siasarId}`}>{record.siasarId}</Link></td>
                    <td>{record.name}</td>
                    <td className={`background-${record.score} text-center`}>{record.score}</td>
                    <td>{record[this.props.indicator]}</td>
                    <td>{record.country}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
        {this.props.children}
      </React.Fragment>
    );
  }
}

Entities.propTypes = {
  entity: PropTypes.string.isRequired,
  indicator: PropTypes.string.isRequired,
  endpoint: PropTypes.string.isRequired,
  stats: PropTypes.bool,
  children: PropTypes.arrayOf(PropTypes.element),
  countryChange: PropTypes.func,
  t: PropTypes.func.isRequired,
};

Entities.defaultProps = {
  stats: false,
  children: null,
  countryChange: null,
};

export default translate()(Entities);
