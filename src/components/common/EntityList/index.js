import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table, Alert, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { translate } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileDownload } from '@fortawesome/free-solid-svg-icons';

import Api from 'lib/Api';
import { formatValue } from 'lib/Utils';

import PageHeader from 'components/common/PageHeader';
import Loading from 'components/common/Loading';
import AdmCrumb from 'components/common/AdmCrumb';
import CountryList from 'components/common/CountryList';
import ScoreList from 'components/common/ScoreList';

class EntityList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      country: null,
      score: null,
      adm: [],
      fullAdm: false,
      records: null,
      total: null,
      limit: 1000,
      params: {},
    };

    this.admChange = this.admChange.bind(this);
    this.countryChange = this.countryChange.bind(this);
    this.scoreChange = this.scoreChange.bind(this);
    this.limitChange = this.limitChange.bind(this);
    this.exportCSV = this.exportCSV.bind(this);
  }

  update() {
    this.setState({
      records: null,
    });

    const params = {
      attributes: ['siasarId', 'name', ...this.props.fields, 'score', 'country', 'location'],
      where: {},
      order: [['surveyDate', 'desc']],
    };

    if (this.state.limit) params.limit = this.state.limit;

    if (this.state.country) params.where.country = this.state.country.code;

    this.state.adm.forEach((value, index) => {
      params.where[`adm${index + 1}`] = value;
    });

    if (this.state.score) {
      params.where.score = this.state.score;
    }

    Promise.all([
      Api.get(this.props.endpoint, params),
      Api.get(`${this.props.endpoint}/count`, { where: params.where }),
    ]).then(results => this.setState({
      records: results[0],
      total: results[1][0].count,
      params,
    }));
  }

  exportCSV() {
    const params = { ...this.state.params };
    params.attributes = {
      exclude: ['surveyYear', 'surveyMonth', 'geom', 'createdAt', 'updatedAt'],
    };
    delete params.limit;
    params.format = 'csv';
    Api.download(this.props.endpoint, params);
  }

  countryChange(country) {
    this.setState({ country, adm: [], fullAdm: false }, this.update);
  }

  scoreChange(score) {
    this.setState({ score }, this.update);
  }

  admChange(adm, fullAdm) {
    if (this.state.records) {
      this.setState({
        adm,
        fullAdm,
      }, this.update);
    }
  }

  limitChange(event) {
    this.setState({ limit: event.target.dataset.limit }, this.update);
  }

  render() {
    const { t } = this.props;
    const table = this.state.records && this.state.records.length ? (
      <React.Fragment>
        <Alert color="info" className="d-flex align-items-center justify-content-between">
          <div>
            {t('showingRecords', { length: this.state.records.length, total: this.state.total })}
            {this.state.limit && this.state.records.length < this.state.total &&
              <Button color="info" size="sm" onClick={this.limitChange} className="ml-2">{t('showAll')}</Button>
            }
          </div>
          <div>
            <Button color="info" size="sm" onClick={this.exportCSV} title="Download CSV">
              <FontAwesomeIcon icon={faFileDownload} fixedWidth />
            </Button>
          </div>
        </Alert>
        <Table size="sm" responsive striped>
          <thead>
            <tr>
              <th>#</th>
              <th>{t('name')}</th>
              {this.props.fields.map(field => (
                <th key={`header-${field}`}>{t(field)}</th>
              ))}
              <th>{t('score')}</th>
              {!this.state.country &&
                <th>{t('country')}</th>
              }
              {!this.state.fullAdm &&
                <th>{t('location')}</th>
              }
            </tr>
          </thead>
          <tbody>
            {this.state.records.map(record => (
              <tr key={record.siasarId}>
                <td><Link to={`/${this.props.endpoint}/${record.siasarId}`}>{record.siasarId}</Link></td>
                <td>{record.name}</td>
                {this.props.fields.map(field => (
                  <td key={`${record.siasarId}-${field}`}>{formatValue(record[field])}</td>
                ))}
                <td className={`background-${record.score} text-center`}>{record.score}</td>
                {!this.state.country &&
                  <td>{record.country}</td>
                }
                {!this.state.fullAdm &&
                  <td className="font-sm">
                    {record.location.slice(this.state.adm.length).filter(adm => adm).join(' / ')}
                  </td>
                }
              </tr>
            ))}
          </tbody>
        </Table>
      </React.Fragment>) : <Alert color="info">{t('noData')}</Alert>;

    return (
      <React.Fragment>
        <PageHeader>
          <h5 className="mr-2">
            { `${this.state.country ? `${this.state.country.name} - ` : ''}
              ${t('listOf', { elements: t(`${this.props.entity}_plural`) })}` }
          </h5>
          <CountryList
            className="mr-2 d-print-none"
            all
            country={this.state.country}
            onChange={this.countryChange}
            endpoint={this.props.endpoint}
          />
          <ScoreList className="d-print-none" all onChange={this.scoreChange} endpoint={this.props.endpoint} />
        </PageHeader>
        {this.state.country &&
          <AdmCrumb country={this.state.country} onChange={this.admChange} endpoint={this.props.endpoint} />
        }
        {this.state.records ? table : <Loading />}
      </React.Fragment>
    );
  }
}

EntityList.propTypes = {
  entity: PropTypes.string.isRequired,
  fields: PropTypes.arrayOf(PropTypes.string).isRequired,
  endpoint: PropTypes.string.isRequired,
  t: PropTypes.func.isRequired,
};

export default translate()(EntityList);
