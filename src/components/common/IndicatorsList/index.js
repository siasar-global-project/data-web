import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListGroup, ListGroupItem } from 'reactstrap';

import { calculateScore } from 'lib/Utils';

class IndicatorsList extends Component {
  render() {
    return (
      <div className="mb-2 font-sm">
        {this.props.title &&
          <h5>{this.props.title}</h5>
        }
        <ListGroup>
          {this.props.items.map(item => (
            <ListGroupItem key={item.title}>
              <span
                className={`circle-score background-${calculateScore(item.value)}`}
              />
              {item.title}
            </ListGroupItem>
          ))}
        </ListGroup>
      </div>
    );
  }
}

IndicatorsList.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
};

IndicatorsList.defaultProps = {
  title: null,
};

export default IndicatorsList;
