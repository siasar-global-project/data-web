import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Collapse, Navbar, Nav } from 'reactstrap';

import { translate } from 'react-i18next';
import LanguageSwitcher from 'components/common/LanguageSwitcher';
import { faChartPie, faHome, faTint, faWrench, faClipboardCheck, faGlassWhiskey, faFileExcel, faSchool, faHospital }
  from '@fortawesome/free-solid-svg-icons';

import NavItem from 'components/common/NavItem';

import iso from 'assets/images/iso.png';

import './style.css';

class Aside extends Component {
  render() {
    const { t } = this.props;
    return (
      <aside className={`aside ${this.props.className} p-0 bg-dark`}>
        <div className="aside-header p-3 bg-primary text-white">
          <img src={iso} alt="SIASAR" className="float-left" />
          <h5 className="ml-5 mb-0">SIASAR Data</h5>
        </div>
        <Navbar color="dark" dark expand className="flex-md-column flex-row align-items-start py-2">
          <Collapse navbar>
            <Nav className="flex-md-column flex-row w-100 justify-content-around" navbar>
              <LanguageSwitcher />
              <NavItem exact to="/" icon={faChartPie} label={t('dashboard')} />
              <NavItem to="/communities" icon={faHome} label={t('community_plural')} />
              <NavItem to="/systems" icon={faTint} label={t('system_plural')} />
              <NavItem to="/service-providers" icon={faWrench} label={t('serviceProvider_plural')} />
              <NavItem to="/technical-providers" icon={faClipboardCheck} label={t('technicalProvider_plural')} />
              <NavItem to="/schools" icon={faSchool} label={t('school_plural')} />
              <NavItem to="/health-centers" icon={faHospital} label={t('healthCenter_plural')} />
              <NavItem to="/sdgs" className="siasarSdg" icon={faGlassWhiskey} label={t('siasarSdg')} />
              <NavItem to="/download-excels" icon={faFileExcel} label={t('downloadExcelByCountry')} />
            </Nav>
          </Collapse>
        </Navbar>
      </aside>
    );
  }
}

Aside.propTypes = {
  className: PropTypes.string,
  t: PropTypes.func.isRequired,
};

Aside.defaultProps = {
  className: null,
};

export default translate()(Aside);
