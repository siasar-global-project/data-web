import qs from 'qs';
import config from 'config';

class Api {
  static async get(url, params = {}) {
    const response = await fetch(Api.buildUrl(url, params));
    const data = await response.json();
    if (!response.ok) {
      if (data.code === '42703') {
        return [];
      }
      throw new Error(data.message);
    }
    return data;
  }

  static async download(url, params = {}) {
    window.location = Api.buildUrl(url, params);
  }

  static buildUrl(url, params = {}) {
    return `${config.siasarApiUrl}/${url}?${qs.stringify(params)}`;
  }
}

export default Api;
