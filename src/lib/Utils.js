export const scoreColors = (alpha = 1) => [
  `rgba(84, 186, 70, ${alpha})`,
  `rgba(255, 255, 57, ${alpha})`,
  `rgba(255, 147, 38, ${alpha})`,
  `rgba(201, 36, 41, ${alpha})`,
  `rgba(100, 100, 100, ${alpha})`,
];

export const entityColors = ['#89AE00', '#0093B4', '#11A891', '#5F5646'];

export const sdgsColors = ['#F7D450', '#FBF176', '#57C3F7', '#3F88D1'];

export const grayColors = [
  'rgba(100, 100, 100, 0.35)',
  'rgba(100, 100, 100, 0.65)',
  'rgba(100, 100, 100, 0.85)',
  'rgba(100, 100, 100, 1)',
];

export const formatFloat = (value = 0) => parseFloat(value).toFixed(2);

export const formatInteger = (value = 0) => Math.round(value);

export const calculateScore = (value) => {
  if (value === null) return 'N';
  if (value >= 0.9 && value <= 1) return 'A';
  if (value >= 0.7 && value < 0.9) return 'B';
  if (value >= 0.4 && value < 0.7) return 'C';
  return 'D';
};

export const scoreColor = (score, alpha = 1) => {
  if (score === 'A') return scoreColors(alpha)[0];
  if (score === 'B') return scoreColors(alpha)[1];
  if (score === 'C') return scoreColors(alpha)[2];
  if (score === 'D') return scoreColors(alpha)[3];
  return scoreColors(alpha)[4];
};

export const randomColors = [
  '#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
  '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
  '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
].sort(() => 0.5 - Math.random());

export const formatValue = (value) => {
  if (typeof value === 'string' ||
    typeof value === 'undefined' ||
    value === null) {
    return value;
  } else if (parseInt(value, 10) === value) {
    return value.toLocaleString('de-DE');
  }
  return formatFloat(value).toLocaleString();
};

export const scores = ['A', 'B', 'C', 'D'];

export const camelToDash = str => str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase();
